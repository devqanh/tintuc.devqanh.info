<?php if ( have_posts() ) : ?>

<?php
	// Create IDS
$tmp = 0;
	$ids1 = array();
	$ids2 = array();
	$ids3 = array();
	while ( have_posts() ) : the_post();
		if($tmp < 2){
			array_push($ids1, get_the_ID());
		}else if($tmp < 5){
			array_push($ids2, get_the_ID());
		}else{
			array_push($ids3, get_the_ID());
		}
	$tmp++;
	endwhile; // end of the loop.
	$ids1 = implode(',', $ids1);
	$ids2 = implode(',', $ids2);
	$ids3 = implode(',', $ids3);
?>

<!----------------2 b�i �?u ti�n--------->
<?php echo do_shortcode('[blog_posts type="masonry" depth="'.flatsome_option('blog_posts_depth').'" depth_hover="'.flatsome_option('blog_posts_depth_hover').'" text_align="left" columns="2" posts="2" ids="'.$ids1.'"]'); ?>
<!----------------3 b�i ti?p theo--------->

<?php echo do_shortcode('[blog_posts style="normal" type="row" col_spacing="small" columns="3" columns__md="1" posts="3" offset="2" title_size="large" show_date="false" excerpt="false" comments="false" image_height="56.25%" text_align="left" text_size="small" ids="'.$ids2.'" ]');?>
<!----------------C�c b�i c?n l?i--------->

<div class="section-2"><?php echo do_shortcode('[blog_posts type="row" image_width="30" depth="'.flatsome_option('blog_posts_depth').'" depth_hover="'.flatsome_option('blog_posts_depth_hover').'" text_align="left"  excerpt_length="45"  style="vertical" offset="2" columns="1" ids="'.$ids3.'"]'); ?></div>

<?php flatsome_posts_pagination(); ?>

<?php else : ?>

	<?php get_template_part( 'template-parts/posts/content','none'); ?>

<?php endif; ?>
